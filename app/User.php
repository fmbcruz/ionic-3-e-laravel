<?php

namespace App;

use \Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class User extends Eloquent {

    protected $connection = 'mongodb';

    /**
     * @var string
     */
    protected $collection = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'age', 'email', 'password', 'telefone',
    ];

}
